# Cookie notification

## Preview(s)

* [Desktop](/res/adblockplus.org/screenshots/cookie-notification-default.png)
* [Tablets (portrait)](/res/adblockplus.org/screenshots/cookie-notification-default-tablets.png)
* [Mobile](/res/adblockplus.org/screenshots/cookie-notification-default-mobile.png)
* [Mobile (Settings page)](/res/adblockplus.org/screenshots/cookie-notification-default-mobile-opened-settings.png)

## Functionality on desktop and tablets

When users visit ABP.org for the first time, they are prompted with a cookie notification bar. The cookie notification bar gives the users information about the fact that we store cookies and the possibility to opt out of tracking cookies.

By default, tracking is enabled on the site, unless the user updates his preferences (SEE BELOW)

Based on how they interact with the cookie notification bar, we have the following scenarios for our users:

### First-time visitors

When users visit ABP.org for the first time, they will see the cookie notification bar at the bottom of their screen. [See previews section >](#previews)

The notification bar is always visible in the viewport, remaining docked to the bottom of the screen as the users scrolls the page up or down. The notification bar remains visible until users specifically click on the `OK, got it` button or the `SAVE PREFERENCES` link (SEE BELOW).

Tracking is enabled by default until the user adjusts his settings.

### Returning visitors

When a user returns to the ABP.org website but he has not specifically clicked on the `OK, got it` button or on the `SAVE PREFERNCES` link in a previous session, the cookie notification bar will be displayed.

If, however, the user has clicked during a previous session on the button, respectively the link mentioned above, the cookie notification bar will not be shown.

By default, tracking is enabled until the user adjusts his settings. The functionality is the same as we have it for [first time visitors](#first-time-visitors).

### Users click on `OK, got it` button

We will refer to the `OK, got it` button as the _dismiss button_ from now on.

When users click on the dismiss button, the cookie notification bar disappears and tracking remains enabled.

Any changes in the cookie settings panel (SEE BELLOW) will not be saved unless the user specifically clicked on the `SAVE PREFERENCES` link. The cookie bar will no longer be shown to users who have clicked on the dismiss button **OR** users who have clicked on the `SAVE PREFERENCES` link. 

### Users click on `Settings` button

A click on the `Settings` button will open the small  _cookie settings panel_ where the users can adjust their cookie preferences. [See preview >](/res/adblockplus.org/screenshots/cookie-notification-opened-settings.png)

Another click on the `Settings` button will close the cookie settings panel and the cookie notification bar will remain visible.

If the cookie settings panel is open and the user clicks outside of it, anywhere on the page, the cookie settings panel will close. The cookie notification bar remains visible.

**Implementation detail:** When the `Settings` button has states _:hover_, _:focus_ or _:active_, the background color of the button is `#EFEFEF`.

There are two types of cookies that we have on ABP.org:

* **Necessary cookies** - these are cookies that remember the user's cookie preferences and cookies that remember if the user has dismissed or updated his preferences (regardless if that happened in the current session or in a past one). These cookies are always enabled in the cookie settings panel and they cannot be disabled by the user.
* **Tracking cookies** - these are cookies used for analyzing website traffic and by default, they are enabled until the user chooses to disable them.

**Detail:** The user can here enable/disable the tracking cookies as he so chooses. However, for any changes to take effect, he must first save his preferences by clicking on the `SAVE PREFERENCES` link.

For users who have opened the cookie settings panel we have the following scenarios:

#### Users disable tracking cookies

After the user opens the cookie settings panel he can choose to disable the tracking cookies. [See preview of disabled state >](/res/adblockplus.org/screenshots/cookie-notification-changed-settings.png)

Once the user disables the tracking cookies, he must also click on `SAVE PREFERENCES` for the changes to take effect. When that happens, the tracking on ABP.org website will be disabled and no tracking cookies will be set via the user's browser. The option will be remembered for the entire current session on ABP.org as well as future sessions.

#### Users click on `SAVE PREFERENCES`

Once the user clicks on the `SAVE PREFERENCES` link from the cookie settings panel, **the cookie notification bar disappears entirely** and the preferences of the user will be remembered for the current and all future sessions on the ABP.org website. 

Depending on the state of the cookie tracking setting, we have the following expected behaviors:

* **User hasn't made any changes:** Tracking remains enabled on all pages of ABP.org.
* **User disabled tracking cookies:** Tracking is disabled on all pages of ABP.org.
* **User disabled cookies, then re-enables them:** Tracking remains enabled on all pages of ABP.org.

## Functionality on mobile devices

For mobile devices, the functionality remains the same as we have on desktop and tablets.

The only difference is in how it looks like and how the cookie settings panel is opened by the user.

By default, the cookie notification is shown minimized. [See preview >](/res/adblockplus.org/screenshots/cookie-notification-default-mobile.png)

There is less content on the mobile version of the cookie notification bar compared to desktop and tablets. Also, instead of `Settings` we have `Change settings`, which functions in the exact same way as the former.

When the users clicks on `Read more` or `Change settings`, the cookie settings panel slides up over the current page the user has opened. Here, the user can update his preferences in [the exact way](#users-disable-tracking-cookies) as he does for desktop and tablets.

[See preview for expaded cookie settings panel >](/res/adblockplus.org/screenshots/cookie-notification-default-mobile-opened-settings.png)

One thing different on mobile devices is the presence of the `X`(close) button, which closes the entire cookie settings panel. If a user clicks the close button, tracking will remain enabled on ABP.org website. If a user disabled tracking cookies and saves his preferences by clicking on the `SAVE PREFERENCES` link, the cookie settings panel closes and tracking will be disabled on ABP.org. In both scenarios, the state of the tracking cookies (enabled/disabled) will be remembered for future visits as well.

## Content

Asset(s):

* Cookie icon
    * [PNG](/res/adblockplus.org/static/cookies_icon.png)
    * [PNG2x](/res/adblockplus.org/static/cookies_icon@2x.png)
    * [SVG](/res/adblockplus.org/static/cookies_icon.svg)
* Settings icon
    * [PNG](/res/adblockplus.org/static/settings_icon.png)
    * [PNG2x](/res/adblockplus.org/static/settings_icon@2x.png)
    * [SVG](/res/adblockplus.org/static/settings_icon.svg)
* Close icon
    * [PNG](/res/adblockplus.org/static/close_icon.png)
    * [PNG2x](/res/adblockplus.org/static/close_icon@2x.png)
    * [SVG](/res/adblockplus.org/static/close_icon.svg)

### Cookie notification (desktop & tablets)

``` 
![Cookie with one bite taken out](cookies_icon.svg) We use some cookies to give you the best experience on our website. By using our site you are aware that we are using cookies and you can change this any time. [Learn more](privacy-policy)

[Settings ![Settings icon](settings_icon.svg)](open settings panel) {: title = "Edit cookie settings" }

[Ok, got it](dismiss notification) {: title = "Dismiss cookie notification" }
```

### Cookie notification (mobiles)

``` 
![Cookie with one bite taken out](cookies_icon.svg) ## Cookie preferences

We use some cookies to give you the best experience on our website. [Read more](open settings panel) {: title = "More information about cookies" }

[Ok, got it](dismiss notification) {: title = "Dismiss cookie notification" }

[Change settings](open settings panel) {: title = "Edit cookie settings" }
```

### Cookie settings panel (desktop & tablets)

``` 
Necessary cookies

Used to remember your privacy preferences. They cannot be switched off.

Tracking cookies

We use these to analyze website traffic.

[SAVE PREFERENCES](Saves any changes and closes the cookie notification) {: title="Save cookie preferences" }
```

### Cookie settings panel (mobile)

``` 
![Cookie with one bite taken out](cookies_icon.svg) ## Cookie preferences ![close icon](close_icon.svg)

We use some cookies to give you the best experience on our website. By using our site you are aware that we are using cookies and you can change this any time. [Learn more](privacy-policy)

Necessary cookies

Used to remember your privacy preferences. They cannot be switched off.

Tracking cookies

We use these to analyze website traffic.

[SAVE PREFERENCES](Saves any changes and closes the cookie notification) {: title="Save cookie preferences" }
```