Filter lists
===========

- [Language filter lists](#language-filter-lists)
- [Non-language filter lists](#non-language-filter-lists)

Other filter lists: https://adblockplus.org/subscriptions

## Non-language filter lists

| Type | Filter list name | Description | URL | Homepage |
|----|----|----|----|----|
| allowing | Allow nonintrusive advertising | N/A | https://easylist-downloads.adblockplus.org/exceptionrules.txt | https://acceptableads.com/ |
| allowing | Allow nonintrusive advertising without third-party tracking | N/A | https://easylist-downloads.adblockplus.org/exceptionrules-privacy-friendly.txt | https://acceptableads.com/ |
| annoyances | Premium: Distraction Control | Block more distractions  | https://easylist-downloads.adblockplus.org/adblock_premium.txt | https://adblock.org/ |
| circumvention | ABP filters | ABP Anti-Circumvention Filter List | https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt | https://github.com/abp-filters/abp-filters-anti-cv |
| notifications | Fanboy's Notifications Blocking List | Block push notifications | https://easylist-downloads.adblockplus.org/fanboy-notifications.txt | https://easylist.to/ |
| privacy | EasyPrivacy | Block additional tracking | https://easylist-downloads.adblockplus.org/easyprivacy.txt | https://easylist.to/ |
| social | Fanboy's Social Blocking List | Block social media icons tracking | https://easylist-downloads.adblockplus.org/fanboy-social.txt | https://easylist.to/ |

## Language filter lists

| Filter list name | Title | URL + EasyList | URL |
|-----------|---------------|---------------|---------------|
| `EasyList` | `English` | https://easylist-downloads.adblockplus.org/easylist.txt | |
| `ABPindo` | `Bahasa Indonesia` | https://easylist-downloads.adblockplus.org/abpindo+easylist.txt | https://raw.githubusercontent.com/ABPindo/indonesianadblockrules/master/subscriptions/abpindo.txt |
| `Bulgarian list` | `български` | https://easylist-downloads.adblockplus.org/bulgarian_list+easylist.txt | https://stanev.org/abp/adblock_bg.txt |
| `EasyList China` | `中文` | https://easylist-downloads.adblockplus.org/easylistchina+easylist.txt | https://easylist-downloads.adblockplus.org/easylistchina.txt |
| `EasyList Czech and Slovak` | `čeština, slovenčina` | https://easylist-downloads.adblockplus.org/easylistczechslovak+easylist.txt | https://raw.github.com/tomasko126/easylistczechandslovak/master/filters.txt |
| `EasyList Dutch` | `Nederlands` | https://easylist-downloads.adblockplus.org/easylistdutch+easylist.txt | https://easylist-downloads.adblockplus.org/easylistdutch.txt |
| `EasyList Germany` | `Deutsch` | https://easylist-downloads.adblockplus.org/easylistgermany+easylist.txt | https://easylist-downloads.adblockplus.org/easylistgermany.txt |
| `EasyList Hebrew` | `עברית` | https://easylist-downloads.adblockplus.org/israellist+easylist.txt | https://raw.githubusercontent.com/easylist/EasyListHebrew/master/EasyListHebrew.txt |
| `EasyList Italy` | `Italiano` | https://easylist-downloads.adblockplus.org/easylistitaly+easylist.txt | |
| `EasyList Lithuania` | `lietuvių kalba` | https://easylist-downloads.adblockplus.org/easylistlithuania+easylist.txt | |
| `EasyList Latvian` | `latviešu valoda` | https://easylist-downloads.adblockplus.org/latvianlist+easylist.txt | https://notabug.org/latvian-list/adblock-latvian/raw/master/lists/latvian-list.txt |
| `EasyList Poland` | ` polski` | https://easylist-downloads.adblockplus.org/easylistpolish+easylist.txt | https://easylist-downloads.adblockplus.org/easylistpolish.txt |
| `EasyList Spanish` | `Español` | https://easylist-downloads.adblockplus.org/easylistspanish+easylist.txt | https://easylist-downloads.adblockplus.org/easylistspanish.txt |
| `EasyList AR` | `العربية` | https://easylist-downloads.adblockplus.org/liste_ar+liste_fr+easylist.txt | https://easylist-downloads.adblockplus.org/Liste_AR.txt |
| `EasyList FR` | `Français` | https://easylist-downloads.adblockplus.org/liste_fr+easylist.txt | https://easylist-downloads.adblockplus.org/liste_fr.txt |
| `ROList` | `Românesc` | https://easylist-downloads.adblockplus.org/rolist+easylist.txt | https://easylist-downloads.adblockplus.org/advblock.txt |
| `Icelandic ABP List` | `íslenska` | https://adblock.gardar.net/is.abp.txt | |
| `void.gr` | `ελληνικά` | https://void.gr/kargig/void-gr-filters.txt | |
| `ABP Japanese Filters` | `日本語` | https://raw.githubusercontent.com/k2jp/abp-japanese-filters/master/abpjf.txt | |
| `ABPVN List` | `Việt` | https://raw.githubusercontent.com/abpvn/abpvn/master/filter/abpvn.txt | |
| `Adblock List for Finland` | `suomi` | http://adb.juvander.net/Finland_adb.txt | |
| `Czech List` | `čeština` | http://adblock.dajbych.net/adblock.txt | |
| `Eesti saitidele kohandatud filter` | `Eesti keel` |  http://adblock.ee/list.php | |
| `hufilter` | `magyar` | https://raw.githubusercontent.com/szpeter80/hufilter/master/hufilter.txt | |
| `YousList` | `한국어` | https://raw.githubusercontent.com/yous/YousList/master/youslist.txt | |
| `IndianList` | `বাংলা (ভারত), ગુજરાતી (ભારત), भारतीय, ਪੰਜਾਬੀ (ਭਾਰਤ), অসমীয়া, मराठी, മലയാളം, తెలుగు, ಕನ್ನಡ, ଓଡ଼ିଆ, नेपाली, සිංහල` | https://easylist-downloads.adblockplus.org/indianlist+easylist.txt | https://easylist-downloads.adblockplus.org/indianlist.txt |
| `Dandelion Sprout's Nordic Filters` | `norsk, dansk, íslenska, føroyskt, kalaallisut` | https://easylist-downloads.adblockplus.org/dandelion_sprouts_nordic_filters+easylist.txt | https://raw.githubusercontent.com/DandelionSprout/adfilt/master/NorwegianExperimentalList%20alternate%20versions/NordicFiltersABP-Inclusion.txt |
